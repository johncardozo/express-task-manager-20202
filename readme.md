# Backend del administrador de tareas

Proyecto hecho en:

- Nodejs
- Express
- Mongodb
- Mongoose

## Instalación

Para instalar este proyecto ejecute:

```bash
npm install
```

## Ejecución

Para ejecutar este proyecto ejecute:

```bash
npm start
```

# Conexion a base de datos

Cree un archivo `.env` y agregue la cadena de conexión a MongoDB con la variable `CONEXION_DB`. Para este proyecto se usó MongoDB Atlas.

