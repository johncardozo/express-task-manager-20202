const mongoose = require("mongoose");

const TareaSchema = mongoose.Schema({
  titulo: {
    type: String,
    required: true,
  },
  descripcion: String,
  fecha: {
    type: Date,
    default: Date.now,
  },
  terminada: {
    type: Boolean,
    default: false,
  },
});

// Exportar el esquema
module.exports = mongoose.model("Tarea", TareaSchema);
