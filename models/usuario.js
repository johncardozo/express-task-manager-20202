const mongoose = require("mongoose");

const UsuarioSchema = mongoose.Schema({
  user: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  nombre: {
    type: String,
    required: true
  }
});

// Exportar el esquema
module.exports = mongoose.model("Usuario", UsuarioSchema);
