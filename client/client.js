const axios = require("axios");

var url = "http://localhost:3000/tareas";

// CREAR TAREA
async function crearTarea() {
  try {
    // Hace la peticion al backend
    let respuesta = await axios.post(url, {
      titulo: "Take photographs",
      descripcion: "Go to the park and take some pictures",
    });
    // Muestra el resultado
    console.log(respuesta.data);
  } catch (error) {
    // Muestra el error
    console.log(respuesta);
  }
}

async function obtenerTodasLasTareas() {
  try {
    // Obtiene todas las tareas
    let tareas = await axios.get(url);
    // Muestra las tareas obtenidas
    console.log(tareas.data);
  } catch (error) {
    console.log(error);
  }
}

async function obtenerTarea(id) {
  try {
    // Obtiene la tarea
    let tarea = await axios.get(`${url}/${id}`);
    // Muestra la tarea
    console.log(tarea.data);
  } catch (error) {
    // Muestra el error
    console.log(error);
  }
}

async function modificarTarea(id) {
  try {
    // Modifica la tarea
    let respuesta = await axios.patch(`${url}/${id}`, { titulo: "Watch TV" });
    // Muestra el resultado
    console.log(respuesta.data);
  } catch (error) {
    // Muestra el error
    console.log(error);
  }
}


async function eliminarTarea(id) {
    try {
      // Elimina la tarea
      let respuesta = await axios.delete(`${url}/${id}`);
      // Muestra el resultado
      console.log(respuesta.data);
    } catch (error) {
      // Muestra el error
      console.log(error);
    }
  }

//crearTarea();
//obtenerTodasLasTareas();
//obtenerTarea("5f9c9104402afa0f99561ea8");
//modificarTarea("5f9c9104402afa0f99561ea8");
eliminarTarea("5f9c9104402afa0f99561ea8");
